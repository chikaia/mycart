<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Category extends Model implements Auditable
{
    protected $fillable = ['parent_id', 'name'];
    public function children()
                    {
    return $this->hasMany('App\Category', 'parent_id');
}
public function products()
  {
    return $this->hasMany('App\Product');
  }

  use \OwenIt\Auditing\Auditable;
}
