<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\User;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::all();
        return view('products.index', compact('products','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('products.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'prod_name'=>'required',
            'prod_desc'=> 'required|max:1999',
            'prod_price' => 'required|integer',
            'prod_pic'=>'image|nullable|max:1999'
          ]);

           //file upload handler
           if($request->hasFile('prod_pic')){
            //get filename with the extension
            $filenameWithExt = $request->file('prod_pic')->getClientOriginalName();
            //get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just ext
            $extension = $request->file('prod_pic')->getClientOriginalExtension();
            //filename to store
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            //image upload
            $path = $request->file('prod_pic')->storeAs('public/prod_pic', $fileNameToStore);

        }else{
            $fileNameToStore = 'noimage.jpg';
        }
        $product = Product::with('audits')->first();
          $product = new Product;
            $product->prod_name = $request->get('prod_name');
            $product->category_id = $request->category_id;
            $product->prod_desc = $request->get('prod_desc');
            $product->prod_price = $request->get('prod_price');
            $product->user_id= auth()->user()->id;
            $product->prod_pic=$fileNameToStore;

          $product->save();
          return redirect('/products')->with('success', 'Product has been added');
        }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $product = Product::find($id);
        return view('products.edit',compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'prod_name'=>'required',
            'prod_desc'=> 'required|max:1999',
            'prod_price' => 'required|integer',
            'prod_pic'=>'image|nullable|max:1999'
          ]);

          $product = Product::find($id);
            $product->prod_name = $request->get('prod_name');
            $product->category_id = $request->category_id;
            $product->prod_desc = $request->get('prod_desc');
            $product->prod_price = $request->get('prod_price');
          $product->save();
          return redirect('/products')->with('success', 'Product has been updated');
        }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect('/products')->with('success', 'Product has been deleted Successfully');
    }
}
