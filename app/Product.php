<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Product extends Model implements Auditable
{
    protected $fillable = [
        'prod_name',
        'prod_desc',
        'prod_price',
        'prod_pic'
    ];

    public function category() {
        return $this->belongsTo('App\Category');
      }

      use \OwenIt\Auditing\Auditable;
}
