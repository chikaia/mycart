@extends('layouts.app')
@section('content')
<style>
    .uper {
      margin-top: 40px;
    }
  </style>
  <div class="container">
  <div class="card uper">
    <div class="card-header">
      Edit Product
    </div>
    <div class="card-body">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
          </ul>
        </div><br />
      @endif
        <form method="post" action="{{ route('products.update',$product->id) }}" enctype="multipart/form-data">
            <div class="form-group">
                @csrf
                @method('PUT')
                <label for="name">Producs Name:</label>
                <input type="text" class="form-control" name="prod_name" value="{{$product->prod_name}}"/>
            </div>
            <div class="form-group">
                <label for="category_id">Category</label>
                <select class="form-control" name="category_id" required>
                  <option value="">Select a Category</option>

                  @foreach ($categories as $category)
                    <option value="{{ $category->id }}" {{ $category->id == $product->category_id ? 'selected' : '' }}>{{ $category->name }}</option>

                    @if ($category->children)
                      @foreach ($category->children as $child)
                        <option value="{{ $child->id }}" {{ $child->id == $product->category_id ? 'selected' : '' }}>&nbsp;&nbsp;{{ $child->name }}</option>
                      @endforeach
                    @endif
                  @endforeach
                </select>
              </div>
            <div class="form-group">
                <label for="description">Producs Description :</label>
                <textarea type="text" class="form-control" name="prod_desc" cols="30" rows="10" >{{$product->prod_desc}}</textarea>
            </div>
            <div class="form-group">
                <label for="price">Product Price:</label>
                <input type="text" class="form-control" name="prod_price" value="{{$product->prod_price}}"/>
            </div>
            <div class="form-group">
                <label for="produc_pic">Product Pic:</label>
                <input type="file"  name="prod_pic"/>
                <br>
                Jpeg images
            </div>
            <button type="submit" class="btn btn-primary">Update Product</button>
        </form>
    </div>
  </div>
  </div>
@endsection
