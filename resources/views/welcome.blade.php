@extends('layouts.app')
@section('content')
<div class="container">
<h1>Welcome to MyCart</h1>
@if(count($products)>0)
@foreach($products as $product)
<div class="card" style="width: 18rem;">
    <img class="card-img-top" src="/storage/prod_pic/{{$product->prod_pic}}" alt="Card image cap">
    <div class="card-body">
      <h2 style="align:center;"> {{$product->prod_name}}</h2>
      <h4><span class="badge badge-dark" >{{$product->category? $product->category->name : 'Uncategorized' }}</span></h4>
      <p><b>KES. {{$product->prod_price}}</b></p>
    </div>
  </div>

  @endforeach
  @else
  <h2>No products Found</h2>
  @endif
</div>
@endsection
